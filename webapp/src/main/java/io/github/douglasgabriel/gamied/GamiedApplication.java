package io.github.douglasgabriel.gamied;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@EnableAutoConfiguration
@SpringBootApplication
public class GamiedApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(GamiedApplication.class, args);
	}

}