package io.github.douglasgabriel.gamied.webapp.controller.user;

import io.github.douglasgabriel.gamied.core.domain.User;
import io.github.douglasgabriel.gamied.manager.service.user.UserService;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/user")
public class UserController {
	
	@Inject
	private UserService service;
	
	@RequestMapping(value = "/find", method = RequestMethod.GET)
	public @ResponseBody User findUser(Long id) {
		return service.find(id);
	}

}
