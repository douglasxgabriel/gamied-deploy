package io.github.douglasgabriel.gamied.manager.service.user;

import io.github.douglasgabriel.gamied.core.domain.User;
import io.github.douglasgabriel.gamied.manager.repository.user.UserRepository;
import io.github.douglasgabriel.gamied.manager.service.GenericServiceImpl;

import javax.inject.Named;

@Named("userServiceDefault")
public class UserServiceImpl extends GenericServiceImpl<User, UserRepository> implements UserService {

}
