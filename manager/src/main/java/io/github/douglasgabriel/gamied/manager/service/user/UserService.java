package io.github.douglasgabriel.gamied.manager.service.user;

import io.github.douglasgabriel.gamied.core.domain.User;
import io.github.douglasgabriel.gamied.manager.service.GenericService;

public interface UserService extends GenericService<User> {
	
}