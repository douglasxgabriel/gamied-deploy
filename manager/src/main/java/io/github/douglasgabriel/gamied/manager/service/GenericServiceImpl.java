package io.github.douglasgabriel.gamied.manager.service;

import io.github.douglasgabriel.gamied.core.PersistableEntity;

import java.util.List;

import javax.inject.Inject;

import org.springframework.data.repository.CrudRepository;

import com.google.common.collect.Lists;

@SuppressWarnings("rawtypes")
public class GenericServiceImpl<T extends PersistableEntity, R extends CrudRepository> implements GenericService<T> {
		
	@Inject
	protected R repository;

	public R getRepository() {
		return repository;
	}

	public void setRepository(R repository) {
		this.repository = repository;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void save(T entity) {
		repository.save(entity);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void remove(T entity) {
		repository.delete(entity);
	}

	@Override
	@SuppressWarnings("unchecked")
	public T find(Long id) {
		return (T) repository.findOne(id);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<T> list() {
		return Lists.newArrayList(repository.findAll());
	}

}
