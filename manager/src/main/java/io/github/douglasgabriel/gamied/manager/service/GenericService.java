package io.github.douglasgabriel.gamied.manager.service;

import io.github.douglasgabriel.gamied.core.PersistableEntity;

import java.util.List;

public interface GenericService<T extends PersistableEntity> {
	
	void save(T entity);
	
	void remove(T entity);
	
	T find(Long id);
	
	List<T> list();

}
