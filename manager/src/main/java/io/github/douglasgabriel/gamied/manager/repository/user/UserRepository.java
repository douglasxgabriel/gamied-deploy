package io.github.douglasgabriel.gamied.manager.repository.user;

import io.github.douglasgabriel.gamied.core.domain.User;

import org.springframework.data.repository.CrudRepository;


public interface UserRepository extends CrudRepository<User, Long>{
	
}
