package io.github.douglasgabriel.gamied.core.domain;

import io.github.douglasgabriel.gamied.core.PersistableEntity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * This class represents an user.
 * 
 * @author douglasgabriel
 * @version 0.1
 */
@Entity
@Table(name = "dmn_user")
@SequenceGenerator(name = "sequence_dmn_user", sequenceName = "sq_dmn_user")
public class User extends PersistableEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator = "sequence_dmn_user")
	private Long id;
	private String name;
	private String email;
	
	public User() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
